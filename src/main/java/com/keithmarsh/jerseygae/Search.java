package com.keithmarsh.jerseygae;

import java.util.Date;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Parent;
import com.keithmarsh.jerseygae.User;

@Entity
public class Search {
	@Id
	Long id;
	@Parent
	Key<User> owner;
	int category;
	String keywords;
	float maxPrice;
	Date latest;
	
	public Search() {
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public Key<User> getOwner() {
		return owner;
	}
	public void setOwner(Key<User> owner) {
		this.owner = owner;
	}
	public int getCategory() {
		return category;
	}
	public void setCategory(int category) {
		this.category = category;
	}
	public String getKeywords() {
		return keywords;
	}
	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}
	public float getMaxPrice() {
		return maxPrice;
	}
	public void setMaxPrice(float maxPrice) {
		this.maxPrice = maxPrice;
	}
	public Date getLatest() {
		return latest;
	}
	public void setLatest(Date latest) {
		this.latest = latest;
	}
}
