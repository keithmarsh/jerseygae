package com.keithmarsh.jerseygae;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("search")
@Produces(MediaType.APPLICATION_JSON)
public class Searches {

    @GET
    @Path("{id}")
    public Search getSearch(@PathParam("id") long id) {
        Search s = new Search();
        s.setId(id);
        return s;
    }
}