package com.keithmarsh.jerseygae;

import java.util.Date;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;

@Entity
public class User {
	@Id
	String id;
	String provider;
	String regStep;
	Date latestListing;
	String email;
	String addressee;
	int maxItemsPerEmail;
	int maxEmailsPerDay;
	int maxSearches;
	
	public User() {
		maxItemsPerEmail = 25;
		maxEmailsPerDay = 5;
		maxSearches = 5;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getProvider() {
		return provider;
	}

	public void setProvider(String provider) {
		this.provider = provider;
	}

	public String getRegStep() {
		return regStep;
	}

	public void setRegStep(String regStep) {
		this.regStep = regStep;
	}

	public Date getLatestListing() {
		return latestListing;
	}

	public void setLatestListing(Date latestListing) {
		this.latestListing = latestListing;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddressee() {
		return addressee;
	}

	public void setAddressee(String addressee) {
		this.addressee = addressee;
	}

	public int getMaxItemsPerEmail() {
		return maxItemsPerEmail;
	}

	public void setMaxItemsPerEmail(int maxItemsPerEmail) {
		this.maxItemsPerEmail = maxItemsPerEmail;
	}

	public int getMaxEmailsPerDay() {
		return maxEmailsPerDay;
	}

	public void setMaxEmailsPerDay(int maxEmailsPerDay) {
		this.maxEmailsPerDay = maxEmailsPerDay;
	}

	public int getMaxSearches() {
		return maxSearches;
	}

	public void setMaxSearches(int maxSearches) {
		this.maxSearches = maxSearches;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", provider=" + provider + ", regStep=" + regStep + ", latestListing=" + latestListing
				+ "]";
	}
}
